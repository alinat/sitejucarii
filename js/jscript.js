'use strict';

$(function()
{
    //configuration
    var width = 720;
    var animationSpeed = 1000;
    var pause = 3000;
    var currentSlide = 1;

    var $slider = $('#slider');
    var $slideContainer = $slider.find('.slides');
    var $slides = $slideContainer.find('.slide')
//copy slide1 to last
    $slideContainer.append($slides.first().clone());
    var interval;

    function startSlider() {
        interval = setInterval(function () {
            $slideContainer.animate({'margin-left': '-=' + width}, animationSpeed, function () {
                currentSlide++;
                if (currentSlide === $slides.length+1) {
                    currentSlide = 1;
                    $slideContainer.css('margin-left', 0);
                }
            });
        }, pause);
}
    function stopSlider()
    {
        clearInterval(interval);
    }
    $slider.on('mouseEnter', stopSlider).on('mouseLeave', startSlider);

    startSlider();
    //animate margin-left
});

$.ajax({
    url: "test.html",
    context: document.body
}).done(function() {
    $( this ).addClass( "done" );
});